#!/bin/bash -e

BALL_VERSION=$1

## standard Unix
git archive --format=tar origin/master contrib/ > contrib-${BALL_VERSION}-master.tar

tmpdir=$(mktemp -d -p .)

cd $tmpdir
tar xf ../contrib-${BALL_VERSION}-master.tar
rm -f ../contrib-${BALL_VERSION}-master.tar

## standard Unix with View
tar cf contrib-${BALL_VERSION}.tar contrib/
gzip -9 contrib-${BALL_VERSION}.tar

## standard Unix, no View
cd contrib

sed -i "s/\(packages=.*\)$/#\1/g" build_contrib
sed -i "s/packages_noview=\(.*\)/packages=\1/g" build_contrib

rm qt-*.tar.gz
rm glew*.tar.gz

cd ..

tar cf contrib-noview-${BALL_VERSION}.tar contrib/
gzip -9 contrib-noview-${BALL_VERSION}.tar

cd ..

mv $tmpdir/*.tar.gz .
rm -rf $tmpdir

## Windows
git archive --format=zip --prefix=BALL_contrib_${BALL_VERSION}/ origin/Windows > contrib-${BALL_VERSION}-Win-source.zip
