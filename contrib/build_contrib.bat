@ECHO OFF

set version="1.4.1"

set packages=boost eigen glew gsl fftw rpc sip

if /i "%1" == "Release" (set build_type=release)
if /i "%1" == "Debug" 	(set build_type=debug)
if /i "%2" == "win32"  	(set platform=win32)
if /i "%2" == "x64"    	(set platform=x64) 
 
if not defined build_type (goto :usage)
if not defined platform   (goto :usage)

rem try to determine the visual studio version
if defined VS90COMNTOOLS  (set vcversion=9)
if defined VS100COMNTOOLS (set vcversion=10)

if not defined vcversion (goto :unknown_vc_version)

md lib
md dlls
md include

REM
REM ====================================================================
REM

REM Building eigen, if required
set build_eigen=false
for %%A in (%packages%) do if "%%A"=="eigen" (goto :build_eigen)

goto :eigen_built

:build_eigen
7z x -y eigen-*.tar.gz
7z x -y eigen-*.tar
cd eigen-eigen-*
xcopy /e /y /i Eigen ..\include\Eigen
cd ..

:eigen_built
echo Eigen built successfully

REM
REM ====================================================================
REM

REM Building GSL, if required
set build_gsl=false
for %%A in (%packages%) do if "%%A"=="gsl" (goto :build_gsl)

goto :gsl_built

:build_gsl
echo Building gsl...
echo Extracting files...
7z x -y gsl*.tar.gz
7z x -y gsl*.tar
cd gsl*
..\7z x -y ..\gsl-patch*.zip
cd build.vc%vcversion%
if /i "%vcversion%" == "9"  (
	msbuild gslhdrs\gslhdrs.vcproj /p:Configuration=%build_type% /p:Platform=%platform%
	msbuild cblasdll\cblasdll.vcproj /p:Configuration=%build_type% /p:Platform=%platform%
	msbuild gsldll\gsldll.vcproj /p:Configuration=%build_type% /p:Platform=%platform%
) else (
	msbuild gslhdrs\gslhdrs.vcxproj /p:Configuration=%build_type% /p:Platform=%platform%
	msbuild gsl.dll.sln /p:Configuration=%build_type% /p:Platform=%platform%
)

copy /y dll\%platform%\%build_type%\*.dll ..\..\dlls
copy /y dll\%platform%\%build_type%\*.pdb ..\..\lib
copy /y dll\%platform%\%build_type%\*.lib ..\..\lib
cd ..
xcopy /e /y /i gsl ..\include\gsl
cd ..

:gsl_built
echo GSL built successfully

REM
REM ====================================================================
REM

REM Building glew, if required
set build_glew=false
for %%A in (%packages%) do if "%%A"=="glew" (goto :build_glew)

goto :glew_built

:build_glew
7z x -y glew-*.zip
cd glew*
cd build
..\..\7z x -y ..\..\glew-patch.zip
cd vc%vcversion%
msbuild glew.sln /p:Configuration=%build_type% /p:Platform=%platform%
cd ..\..
xcopy /e /y /i include\GL ..\include\GL
copy /y lib\*.lib ..\lib
copy /y bin\*.dll ..\dlls
cd ..

:glew_built
echo GLew built successfully

REM
REM ====================================================================
REM

REM Building boost, if required
set build_boost=false
for %%A in (%packages%) do if "%%A"=="boost" (goto :build_boost)

goto :boost_built

:build_boost
7z x -y bzip2*.tar.gz
7z x -y bzip2*.tar
7z x -y bzip2-patch.tar.gz
7z x -y bzip2-patch.tar
7z x -y zlib*.tar.gz
7z x -y zlib*.tar
7z x -y boost*.7z
cd boost*
call bootstrap.bat 

set boost_variant=release
if /i "%build_type%"=="debug" (set boost_variant=debug)

set boost_address_model=32
if /i "%platform%"=="x64" (set boost_address_model=64)

bjam link=shared include=../include --prefix=%~dp0 --with-system --with-thread --with-date_time --with-regex --with-serialization --with-iostreams -sBZIP2_SOURCE=%~dp0/bzip2-1.0.6 -sZLIB_SOURCE=%~dp0/zlib-1.2.5 threading=multi variant=%boost_variant% address-model=%boost_address_model% install

cd ..
move include\boost*\boost include\boost
move lib\*.dll dlls
rd /s /q include\boost-*

:boost_built
echo boost built successfully

REM Building fftw, if required
set build_fftw=false
for %%A in (%packages%) do if "%%A"=="fftw" (goto :build_fftw)

goto :fftw_built

:build_fftw
7z x -y fftw-3.2.2.tar.gz 
7z x -y fftw-3.2.2.tar 
cd fftw*
..\7z x -y ..\fftw-3.2.2-libs-visual-studio-2010.zip
cd fftw-3.2.2-libs
msbuild fftw-3.2.2-libs.sln /p:Configuration=%build_type% /p:Platform=%platform%
copy /y %build_type%\*.dll ..\..\dlls
copy /y %build_type%\*.pdb ..\..\lib
copy /y %build_type%\*.lib ..\..\lib
cd ..
copy /y api\fftw3.h ..\include
cd ..

:fftw_built
echo fftw built successfully

REM Building oncrpc, if required
set build_rpc=false
for %%A in (%packages%) do if "%%A"=="rpc" (goto :build_rpc)

goto :rpc_built

:build_rpc
7z x -y oncrpc.zip
cd oncrpc
md build
cd build
set oncrpc_generator=Visual Studio

if /i %vcversion%==9 (
	set oncrprc_generator=%oncrpc_generator% 9 2008
) else (
	set oncrpc_generator=%oncrpc_generator% 10
)

if /i "%platform%"=="x64" (
	set oncrpc_generator=%oncrpc_generator% Win64
)

cmake .. -G"%oncrpc_generator%"

msbuild ONCRPC.sln /p:Configuration=%build_type% /p:Platform=%platform%
copy /y %build_type%\*.dll ..\..\dlls
copy /y %build_type%\*.lib ..\..\lib
copy /y %build_type%\*.pdb ..\..\lib
cd ..
xcopy /e /y /i rpc ..\include\rpc
cd ..

:rpc_built
echo rpc built successfully

REM Building sip, if required
set build_sip=false
for %%A in (%packages%) do if "%%A"=="sip" (goto :build_sip)

goto :sip_built

:build_sip
7z x -y sip*.zip
cd sip*
if /i %build_type%==release (python configure.py)
if /i %build_type%==debug (python configure.py -u)
nmake
copy sipgen\sip.exe ..\bin
copy siplib\*.pyd ..\dlls
copy siplib\*.lib ..\lib
copy siplib\*.pdb ..\lib
copy siplib\*.h ..\include
cd ..

:sip_built
echo SIP built successfully

echo Building archive
rd /s /q BALL_contrib_%version%
del BALL_contrib_%version%.zip
md BALL_contrib_%version%
xcopy /e /y /i bin BALL_contrib_%version%\bin
xcopy /e /y /i dlls BALL_contrib_%version%\dlls
xcopy /e /y /i include BALL_contrib_%version%\include
xcopy /e /y /i lib BALL_contrib_%version%\lib
xcopy /e /y /i share BALL_contrib_%version%\share
7z a BALL_contrib_%version%.zip BALL_contrib_%version%
rd /s /q BALL_contrib_%version%

goto :eof

:unknown_vc_version
echo "Could not determine vc version number! Are you running in a VS command shell?"

:usage
echo "Usage: build_contrib.bat [Debug|Release] [win32|x64]"

