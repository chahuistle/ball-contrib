#!/bin/bash

# replace local with absolute paths in the binaries for MacOS X

abs_dir=$(pwd)

for i in lib/*.dylib; do
	libs=$(otool -L $i | grep $(basename $i) | tail -n +2); 
	if [ -n "$libs" ]; then 
		lib=$(echo $libs | tr -d ":" | awk '{print $1}')
		# let's see if it is already an absolute path
		case $lib in
			/*) echo "Path $lib is already absolute";;
			 *) abs_path=$abs_dir/$i
                            echo "Replacing relative path $lib by absolute path $abs_path"
			    install_name_tool -id $abs_path $i
			    ;;
		esac
	fi
done
